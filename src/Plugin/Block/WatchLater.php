<?php
/**
 * Created by PhpStorm.
 * User: geertdegezelle
 * Date: 7/06/18
 * Time: 14:03
 */

namespace Drupal\watch_later\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Defines a watch later list block.
 *
 * @Block(
 *   id = "watch_later_block",
 *   admin_label = @Translation("Watch Later")
 * )
 *
 * @package Drupal\watch_later\Plugin\Block
 */
class WatchLater extends BlockBase implements ContainerFactoryPluginInterface {

  protected $formBuilder;
  protected $routeMatch;
  protected $user;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id, $plugin_definition,
    FormBuilderInterface $form_builder,
    RouteMatchInterface $route,
    AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->routeMatch = $route;
    $this->user = $current_user;
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   *
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /**
     * We use new static() instead of new self() in order to allow us to extend BlockBase more than once
     */
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('current_route_match'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    //$route = \Drupal::routeMatch();
    $route = $this->routeMatch;
    $node = $route->getParameter('node');
    $nid = $node->id();
    //kint($this->user);

    if (!$this->user->isAnonymous()) {
      $uid = $this->user->id();
      $form = $this->formBuilder->getForm('Drupal\watch_later\Form\WatchLaterNodeForm', $nid, $uid);
      $build[] = $form;
    }
    else {
      $build[] = [
        '#markup' => t('You have to be logged in to add an article to your read later list.'),
      ];
    }

    return $build;
  }

}