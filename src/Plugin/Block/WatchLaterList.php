<?php
/**
 * Created by PhpStorm.
 * User: geertdegezelle
 * Date: 8/06/18
 * Time: 12:14
 */

namespace Drupal\watch_later\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a watch later list block.
 *
 * @Block(
 *   id = "watch_later_list_block",
 *   admin_label = @Translation("Watch Later Nodes List")
 * )
 *
 * @package Drupal\watch_later\Plugin\Block
 */
class WatchLaterList extends BlockBase implements ContainerFactoryPluginInterface {

  protected $user;

  protected $database;

  protected $manager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $current_user, Connection $database, EntityTypeManager $manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->user = $current_user;
    $this->database = $database;
    $this->manager = $manager;
    $this->setConfiguration($configuration);
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager')
    );

  }

  public function build() {
    $build = [
      '#cache' => [
        'context' => ['user'],
        'tags' => ['watch_later_list:' . $this->user->id()]
      ]
    ];

    $nids = [];


    if (!$this->user->isAnonymous()) {
      $uid = $this->user->id();


      // Add user cache tags
      $renderer = \Drupal::service('renderer');
      $renderer->addCacheableDependency($build, $this->user);

      $query = $this->database->select('watch_later', 'wl')
        ->fields('wl', ['nid'])
        ->condition('uid', $uid, '=');
      $nids = $query->execute()->fetchCol();

      if (!empty($nids)) {
        $nodes = $this->manager->getStorage('node')->loadMultiple($nids);
        $rows = [];

        foreach ($nodes as $node) {
          $rows[$node->id()][] = $node->label();
          $rows[$node->id()][] = $node->toLink('View row');

        }

        $build[] = [
          '#theme'=>'table',
          '#header' => array('Node', 'Action'),
          '#rows' => $rows
        ];

      }
      else {
        $build[] = ['#markup' => t('No items')];
      }
    }

    return $build;
  }

}