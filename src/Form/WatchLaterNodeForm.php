<?php
/**
 * Created by PhpStorm.
 * User: geertdegezelle
 * Date: 7/06/18
 * Time: 15:56
 */

namespace Drupal\watch_later\Form;


use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\watch_later\WatchLaterStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WatchLaterNodeForm extends FormBase {

  protected $storage;

  /**
   * WatchLaterNodeForm constructor.
   *
   * @param \Drupal\watch_later\WatchLaterStorageInterface $storage
   *
   */
  public function __construct(WatchLaterStorageInterface $storage) {
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('watch_later.storage')
    );
  }

  public function getFormId() {
    return 'watch_later_node_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $build_info = $form_state->getBuildInfo();
    $build_info['args'];
    $nid = $build_info['args'][0];
    $uid = $build_info['args'][1];

    // Controle of het artikel in de data zit
    // table aanmaken

    $submit_value = t('Add article');
    if ($this->storage->isInList($nid, $uid)) {
      $submit_value = t('Remove article');
    }

    $form['actions']  = [];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $submit_value,
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $build_info['args'];
    $nid = $build_info['args'][0];
    $uid = $build_info['args'][1];

    $connection = \Drupal::database();

    if ($this->storage->isInList($nid, $uid)) {
      $connection->delete('watch_later')
        ->condition('nid', $nid)
        ->condition('uid', $uid)
        ->execute();
    }
    else {
      $query = $connection->merge('watch_later')
        ->fields(['nid', 'uid'], [$nid, $uid])
        ->condition('nid', $nid)
        ->condition('uid', $uid);
      $query->execute();


    }
    Cache::invalidateTags(['watch_later_list:' . $uid ]);
  }
}