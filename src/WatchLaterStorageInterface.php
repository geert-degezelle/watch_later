<?php
/**
 * Created by PhpStorm.
 * User: geertdegezelle
 * Date: 8/06/18
 * Time: 11:00
 */

namespace Drupal\watch_later;


interface WatchLaterStorageInterface {

  /**
   *
   * Check if an article is in the watch later list
   * @param int $nid
   * @param int $uid
   *
   * @return bool
   */
  public function isInList($nid, $uid);

}