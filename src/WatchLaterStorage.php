<?php
/**
 * Created by PhpStorm.
 * User: geertdegezelle
 * Date: 8/06/18
 * Time: 10:59
 */

namespace Drupal\watch_later;


use Drupal\Core\Database\Connection;

class WatchLaterStorage implements WatchLaterStorageInterface {

  protected $connection;

  /**
   * WatchLaterStorage constructor.
   *
   * @param \Drupal\watch_later\Connection $connection
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function isInList($nid, $uid) {
    $result = $this->connection->select('watch_later', 'wl')
      ->fields('wl', ['nid'])
      ->condition('nid', $nid)
      ->condition('uid', $uid)
      ->execute()
      ->fetch();
    return !empty($result);
  }
}